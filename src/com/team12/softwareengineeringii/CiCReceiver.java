package com.team12.softwareengineeringii;

import org.eclipse.paho.client.mqttv3.MqttException;

// Class Overview:
//     - This class implements Runnable in order to allow it to be used for multithreading. It creates 1 MQTT drone,
//    *CiC-Command*. This keeps the Command-in-Control's on demand updates, requests & fulfillment in a separate thread.
public class CiCReceiver implements Runnable
{
    @Override
    public void run()
    {
        System.out.println("\n*******|A new thread has begun: " + Thread.currentThread().getName() + "|*******");
        //MQTT implementation on Diagnostic Thread
        Drone drone4 = new Drone("[CiC]", "CiC-Command");

        try
        {
            drone4.connect();

            while (true) {

                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Disconnection for all drones
        finally
        {
//            drone4.disconnectDrone();
            if (drone4.isConnected()) {
                try {
                    drone4.client.disconnect();
                } catch (MqttException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}