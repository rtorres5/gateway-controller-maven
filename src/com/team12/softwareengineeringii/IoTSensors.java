package com.team12.softwareengineeringii;

import org.eclipse.paho.client.mqttv3.MqttException;

// Class Overview:
//    - This class implements Runnable in order to allow it to be used for multithreading. It creates the 3 MQTT drones
//		(*[Cabin Sensor]*, *[Engine Sensor]*, and *[External Sensor]*). This keeps the IoT sensor data and posting on a
//		thread by itself.
public class IoTSensors implements Runnable
{
	@Override
	public void run() {
		System.out.println("\n*******|A new thread has begun: " + Thread.currentThread().getName() + "|*******");
		Drone drone1 = new Drone("[Cabin Sensor]", "IoT-SWEII-BoeingOps-cabin");
		Drone drone2 = new Drone("[External Sensor]", "IoT-SWEII-BoeingOps-external");
		Drone drone3 = new Drone("[Engine Sensor]", "IoT-SWEII-BoeingOps-engine");


		try {
			drone1.connect();
			//Thread.sleep(5000);
			drone2.connect();
			//Thread.sleep(5000);
			drone3.connect();
			//Thread.sleep(5000);


			while (true) {

				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}


		// Disconnection for all drones
		finally {

			if (drone1.isConnected()) {

				try {
					drone1.client.disconnect();
				} catch (MqttException e) {
					e.printStackTrace();
				}
			}

			if (drone2.isConnected()) {

				try {
					drone2.client.disconnect();
				} catch (MqttException e) {
					e.printStackTrace();
				}
			}

			if (drone3.isConnected()) {

				try {
					drone3.client.disconnect();
				} catch (MqttException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
