package com.team12.softwareengineeringii;

// Class Overview
//    - This class implements Runnable in order to allow it to be used for multithreading. It orders the gateway heartbeat
//    from Gateway Diagnostics according to the GWController's heartbeat according to the *heartbeatFrequency* attribute.
//    This keeps the heartbeat reporting separate from on-demand testing.
public class HeartbeatManager implements Runnable {
    @Override
    public void run()
    {
        System.out.println("\n*******|A new thread has begun: " + Thread.currentThread().getName() + "|*******");
        while (true)
        {
            try
            {
                Thread.sleep(GWController.heartbeatFrequency);
                System.out.println("\n********************|Heartbeat Start|**********************************");
                System.out.println("Sending request to Diagnostics: GATEWAY HEARTBEAT");
                String diagnosticResponse = GWController.orderSingleTest("gatewayHB", String.valueOf(GWController.gatewayID));
                System.out.println("Diagnostics responded with: " + diagnosticResponse);
                GWControllerHTTPSHandler.sendPOST(diagnosticResponse, "gatewayHB", GWController.heartbeatURL);
                System.out.println("Diagnostic Response was sent as a POST to the server!");
                System.out.println("********************|Heartbeat End|************************************");
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
    }
}
