package com.team12.softwareengineeringii;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.net.URL;
import java.util.ArrayList;

// Class Overview:
// - This is the only class with a main method, and is what needs to be run to turn on the Gateway Controller.
// - Creates a GWController instance, that connects to the Gateway Diagnostic component.
// - For this class to operate correctly, the Python program (*gateway_diagnostics.py*) that complements it must be
//   started first on the same local machine, to create a socket that it can use to fulfill heartbeat requests and on
//   demand test requests.
// - All helper methods are used to order tests from the Gateway Diagnostics component.


public class GWController
{

    public static int gatewayID;
    public static int locationID;
    public static String owner;
    public static int heartbeatFrequency;
    public static Socket socket;
    public static DataOutputStream dataOut;
    public static DataInputStream dataIn;
    public static URL heartbeatURL;
    public static URL dailyDiagnosticURL;
    public static URL customRequestURL;


    public GWController(int gatewayID, int locationID, String owner, int heartbeatFrequency, int portNumber)
    {
        try {

            this.gatewayID = gatewayID;
            this.locationID = locationID;
            this.owner = owner;
            this.heartbeatFrequency = heartbeatFrequency;
            this.socket = new Socket("localhost", portNumber);
            this.dataOut = new DataOutputStream(socket.getOutputStream());
            this.dataIn = new DataInputStream(socket.getInputStream());

            // API Routes for Cloud Server
            this.heartbeatURL = new URL("https://team12.softwareengineeringii.com/api/gatewayHB/createGatewayHB");
            this.dailyDiagnosticURL = new URL("https://team12.softwareengineeringii.com/api/dailyDiagnostic/createGatewayDailyDiagnostic");
            this.customRequestURL = new URL("https://team12.softwareengineeringii.com/api/customRequest/createCustomRequest");
        }
        catch (Exception e)
        {
            System.out.println("Your Gateway Controller did not get instantiated!!!!");
            e.printStackTrace();
        }
    }

    // The gateway controller operates using 3 Threads to fulfil its functions.
    // 1. Plane Sensor Thread
    //    - This thread manages the IoT sensor data. Data is received, then posted via HTTPS to the database.
    // 2. Heartbeat Manager Thread
    //    - This thread is in charge of sending Command-in-Control a "heartbeat" which will allow the Command-in-Control to
    //    know if the gateway is turned on. This frequency is able to be changed by the Command-in-Control via MQTT.
    // 3. This thread manages all incoming order from Command-in-Control.
    //    - This includes:
    //      1. Daily Diagnostics
    //      2. Custom Tests (Not fully implemented)
    //      3. Update Gateway Owner
    //      4. Update Gateway ID
    //      5. Update Gateway Location
    //      6. Update Heartbeat frequency

    public static void main (String [] args)
    {
        System.out.println("\n**************|Gateway Controller Setup Begun|************************");
        GWController controller = new GWController(1, 101, "Adam the Admin", 60000, 2004);

        System.out.println("Your Gateway Controller has been initialized.");
        System.out.println("Gateway: " + controller.gatewayID + "\nLocation: " + controller.locationID +
                "\nOwner: " + controller.owner);

        IoTSensors iotThread = new IoTSensors();
        Thread sensorThread = new Thread(iotThread);
        sensorThread.setName("Plane Sensor Thread");

        CiCReceiver receiverThread = new CiCReceiver();
        Thread cicThread = new Thread(receiverThread);
        cicThread.setName("Command-in-Control Receiver Thread");

        HeartbeatManager hbThread = new HeartbeatManager();
        Thread heartbeatThread = new Thread(hbThread);
        heartbeatThread.setName("Heartbeat Manager Thread");

        // Start all threads
        cicThread.start();
        heartbeatThread.start();
        sensorThread.start();

    }

    // Function Name: prepareSingleRequest
    // Parameters:
    //             1. diagnosticTestName - a String that represents the name of a diagnostic test
    //             2. gatewayId - an int that corresponds to the GWController's current Id attribute
    // Purpose: Prepares a diagnostic test request as a JSON object and transforms it into a string.
    //			Returns the value of the String to the user.


    public static String prepareSingleRequest(String diagnosticTestName, String gatewayId)
    {
        JSONObject diagnosticMessage = new JSONObject();
        diagnosticMessage.put("request", diagnosticTestName);
        diagnosticMessage.put("gateway_id", gatewayId);
        String requestAsString = diagnosticMessage.toString();

        return requestAsString;
    }

    // Function Name: orderDiagnosticTest
    // Parameters:
    //            1. testName - a String that represents the name of a diagnostic test
    //            2. gatewayId - an int that corresponds to the GWController's current Id attribute
    // Purpose: Prepares a diagnostic test request as JSON, then writes it as UTF to the dataOutputStream
    // 			of the GatewayControllerSocketClient object. Receives a UTF response from Gateway Diagnostics
    //			as a string in JSON format. Return that String to the user.


    public static String orderSingleTest(String testName, String gatewayId) {
        String responseFromDiagnostic = "";
        System.out.println("gatewayid reference in order singletest is: " + gatewayId);

        try {
            String diagnosticMessage = prepareSingleRequest(testName, gatewayId);
            dataOut.writeUTF(diagnosticMessage);
            dataOut.flush();

            responseFromDiagnostic = dataIn.readUTF();
        } catch (Exception e) {
            System.out.println("Single Diagnostic test did not complete due to bad inputs.");
            e.printStackTrace();
        }

        return responseFromDiagnostic;
    }

    // Function Name: orderCustomTest
    // Parameters:
    //            1. customTestList - an ArrayList of Strings that represents the name each diagnostic test
    //            2. gatewayId - an int that corresponds to the GWController's current Id attribute
    // Purpose: Accepts a custom test request then writes it as UTF to the dataOutputStream
    // 			of the GatewayControllerSocketClient object. Receives a UTF response from Gateway Diagnostics
    //			as a string in JSON format. Return that String to the user.

    public static String orderCustomTest(ArrayList<String> customTestList, String gatewayId) {
        String responseFromDiagnostic = "";

        try {
            String diagnosticMessage = prepareCustomRequest(customTestList, gatewayId);
            dataOut.writeUTF(diagnosticMessage);
            dataOut.flush();

            responseFromDiagnostic = dataIn.readUTF();
        } catch (Exception e) {
            System.out.println("Custom diagnostic test did not complete due to bad inputs.");
            e.printStackTrace();
        }

        return responseFromDiagnostic;
    }


    // Function Name: orderCustomTest
    // Parameters:
    //            1. customTestList - an ArrayList of Strings that represents the name each diagnostic test
    //            2. gatewayId - an int that corresponds to the GWController's current Id attribute
    // Purpose: Prepares a custom diagnostic test request as JSON, in preparation for the diagnostic test request.
    //          After the JSONObject is created, it is returned as a string value.

    public static String prepareCustomRequest(ArrayList<String> customTestList, String gatewayId) {
        JSONObject diagnosticMessage = new JSONObject();
        diagnosticMessage.put("request", "custom_request");
        diagnosticMessage.put("gateway_id", gatewayId);
        JSONArray testListJSONArray = new JSONArray();

        for (int index = 0; index < customTestList.size(); index++) {
            testListJSONArray.add(customTestList.get(index));
        }

        String arrayAsString = testListJSONArray.toString();
        diagnosticMessage.put("custom_request", arrayAsString);
        String requestAsString = diagnosticMessage.toString();

        return requestAsString;
    }


}
