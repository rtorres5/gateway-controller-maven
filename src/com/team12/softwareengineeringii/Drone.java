package com.team12.softwareengineeringii;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttAsyncClient;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

// Class Overview:
// - This class was designed by Oracle to facilitate MQTT protocols. It has been modified significantly to react
//	 operate on Mosquitto Test Broker, and to react specifically to the MQTT topics that were agreed upon for
//	 communication between IoT Sensors and Command-in-Control.
// - This class allows the instantiation of 'Drone' objects that subscribe/publish to our preset MQTT Topics.

public class Drone implements MqttCallback, IMqttActionListener {

	private String topic;

	public static final String ENCODING = "UTF-8";

	// Quality of Service = Exactly once
	// I want to receive all messages exactly once
	public static final int QUALITY_OF_SERVICE = 2;
	protected String name;
	protected String clientId;
	public String diagnosticTest;
	protected MqttAsyncClient client;
	protected MemoryPersistence memoryPersistence;
	protected IMqttToken connectToken;
	protected IMqttToken subscribeToken;
	public String successCode = "200";
	public String errorCode = "404";

	public Drone(String name, String topic) {
		this.name = name;
		this.topic = topic;
	}

	public String getName() {
		return name;
	}

	public void connect() {

		try {
			MqttConnectOptions options = new MqttConnectOptions();

			memoryPersistence = new MemoryPersistence();
			String serverURI = "tcp://test.mosquitto.org:1883";


			clientId = MqttAsyncClient.generateClientId();
			client = new MqttAsyncClient(serverURI, clientId, memoryPersistence);
			client.setCallback(this);
			connectToken = client.connect(options, null, this);
		} catch (MqttException e) {
			e.printStackTrace();
		}
	}

	public boolean isConnected() {
		return (client != null) && (client.isConnected());
	}

	@Override
	public void connectionLost(Throwable cause) {
		// The MQTT client lost the connection
		cause.printStackTrace();
	}

	@Override
	public void onSuccess(IMqttToken asyncActionToken) {
		if (asyncActionToken.equals(connectToken)) {
			System.out.println(String.format("%s successfully connected", name));

			try {
				subscribeToken = client.subscribe(topic, QUALITY_OF_SERVICE, null, this);
			} catch (MqttException e) {
				e.printStackTrace();
			}
		} else if (asyncActionToken.equals(subscribeToken)) {
			System.out.println(String.format("%s subscribed to the %s topic", name, topic));
			publishTextMessage(String.format("%s is listening.", name));
		}
	}

	@Override
	public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
		// The method will run if an operation failed
		exception.printStackTrace();
	}

	public MessageActionListener publishTextMessage(String messageText) {
		byte[] bytesMessage;

		try {
			bytesMessage = messageText.getBytes(ENCODING);
			MqttMessage message;
			message = new MqttMessage(bytesMessage);
			String userContext = "ListeningMessage";
			MessageActionListener actionListener = new MessageActionListener(topic, messageText, userContext);
			client.publish(topic, message, userContext, actionListener);
			return actionListener;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		} catch (MqttException e) {
			e.printStackTrace();
			return null;
		}
	}


	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		try {
			URL cabinSensorURL = new URL("https://team12.softwareengineeringii.com/api/cabinSensor/createCabinSensor");
			URL externalSensorURL = new URL("https://team12.softwareengineeringii.com/api/externalSensor/createExternalSensor");
			URL engineSensorURL = new URL("https://team12.softwareengineeringii.com/api/engineSensor/createEngineSensor");

			// A message has arrived from the MQTT broker
			// The MQTT broker doesn't send back
			// an acknowledgment to the server until
			// this method returns cleanly
			if (!topic.equals(topic)) {
				this.publishTextMessage(errorCode);
				return;
			}

			String messageText = new String(message.getPayload(), ENCODING);
			if (messageText.equals(successCode) || (messageText.equals(errorCode)))
			{
				return;
			}

			System.out.println(String.format("%s received %s: %s", name, topic, messageText));
			//String[] keyValue = messageText.split(COMMAND_SEPARATOR);


			if (messageText.equals("[Cabin Sensor] is listening.") || (messageText.equals("[External Sensor] is listening.")) ||
					(messageText.equals("[Engine Sensor] is listening.")) || (messageText.equals("[CiC] is listening.")))
			{
				System.out.println("\n***********|New Listener for topic: " + topic + "|************************");
			}

			else if (topic.equals("IoT-SWEII-BoeingOps-cabin"))
			{
				System.out.println("\n***************|Cabin Sensor Start|*********************************");
				GWControllerHTTPSHandler.sendPOST(messageText, "cabin_sensor", cabinSensorURL);
				System.out.println("Cabin Sensor POST is complete!");
				System.out.println("***************|Cabin Sensor End|***********************************");
			}

			else if (topic.equals("IoT-SWEII-BoeingOps-external"))
			{
				System.out.println("\n***************|External Sensor Start|*******************************");
				GWControllerHTTPSHandler.sendPOST(messageText, "external_sensor", externalSensorURL);
				System.out.println("External Sensor POST is complete!");
				System.out.println("***************|External Sensor End|*********************************");
			}

			else if (topic.equals("IoT-SWEII-BoeingOps-engine"))
			{
				System.out.println("\n***************|Engine Sensor Start|**********************************");
				GWControllerHTTPSHandler.sendPOST(messageText, "engine_sensor", engineSensorURL);
				System.out.println("Engine Sensor POST is complete!");
				System.out.println("***************|Engine Sensor End|*************************************");
			}

			else if (topic.equals("CiC-Command"))
			{
				System.out.println("\n***************|CiC Request Start|***************************************");
				System.out.println("CiC sent an MQTT message: " + messageText);
				String[] stringArray = messageText.split(":");
				ArrayList<String> cicArray = new ArrayList(Arrays.asList(stringArray));
				System.out.println("Size of cicArray: " + cicArray.size());
				System.out.println(cicArray.get(0));
				String indexZero = cicArray.get(0);

				if (cicArray.get(0).equals("ID"))
				{
					int newGatewayID = Integer.parseInt(cicArray.get(1));
					GWController.gatewayID = newGatewayID;
					System.out.println("Gateway Controller has successfully updated the GATEWAY ID");
					cicArray.clear();
					System.out.println("******************|CiC Request End|***************************************");
					this.publishTextMessage(successCode);
				}

				else if (cicArray.get(0).equals("OWNER")) {
					GWController.owner = cicArray.get(1);
					System.out.println("Gateway Controller has successfully updated the OWNER.");
					cicArray.clear();
					System.out.println("*******************|CiC Request End|*************************************");
					this.publishTextMessage(successCode);
				}
				else if (cicArray.get(0).equals("LOCATION"))
				{
					int newLocation = Integer.parseInt(cicArray.get(1));
					GWController.locationID = newLocation;
					System.out.println("Gateway Controller has successfully updated the LOCATION ID.");
					cicArray.clear();
					System.out.println("*******************|CiC Request End|*************************************");
					this.publishTextMessage(successCode);
				}
				else if (cicArray.get(0).equals("HEARTBEAT")) {
					int newFrequency = Integer.parseInt(cicArray.get(1));
					GWController.heartbeatFrequency = newFrequency;
					System.out.println("Gateway Controller has successfully updated the HEARTBEAT FREQUENCY.");
					cicArray.clear();
					System.out.println("*******************|CiC Request End|*************************************");
				}
				else if (cicArray.get(0).equals("DAILY_DIAGNOSTIC")) {
					System.out.println("Sending request to Diagnostics: DAILY DIAGNOSTIC");
					String diagnosticResponse = GWController.orderSingleTest("daily_diagnostic", String.valueOf(GWController.gatewayID));
					System.out.println("Diagnostics responded with: " + diagnosticResponse);
					GWControllerHTTPSHandler.sendPOST(diagnosticResponse, "daily_diagnostic", GWController.dailyDiagnosticURL);
					System.out.println("Diagnostic Response was sent as a POST to the server!");
					cicArray.clear();
					System.out.println("*******************|CiC Request End|*************************************");
					this.publishTextMessage(successCode);
				}
				else if (cicArray.get(0).equals("CUSTOM")) {
					cicArray.remove(0);
					System.out.println("Sending request to Diagnostics: CUSTOM TESTS LIST");
					String diagnosticResponse = GWController.orderCustomTest(cicArray, String.valueOf(GWController.gatewayID));
					System.out.println("Diagnostics responded with: " + diagnosticResponse);
					GWControllerHTTPSHandler.sendPOST(diagnosticResponse, "custom_request", GWController.customRequestURL);
					System.out.println("Diagnostic Response was sent as a POST to the server!");
					cicArray.clear();
					System.out.println("*******************|CiC Request End|*************************************");
					this.publishTextMessage(successCode);
				}
			}

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("A MalformedURL exception has occurred! Check the way you set up your HTTPS request.");
		}

	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
		// Delivery for a message has been completed
		// and all acknowledgments have been received

	}

	public void disconnectDrone() {
		if (this.isConnected()) {
			try {
				this.client.disconnect();
			} catch (MqttException e) {
				e.printStackTrace();
			}
		}
	}
}
