# Gateway Controller

## Overview

The gateway controller is a processing hub for IoT Device sensor data that needs to enter the Command-in-Control's 
database.

## Communication Methods  

The gateway controller 3 types of communication.
 
1) HTTPS  
   - This is how the gateway controller sends sensor data from IoT devices to the Cloud.

2) Socket Server  
   - This is how the gateway controller sends Diagnostic Test Requests to the Python program that is on the same local machine. 

3) MQTT  
   - This is how the gateway controller receives messages from Command-in-Control and also how the gateway controller receives IoT sensor data.

## Operating Process

The gateway controller operates using 3 Threads to fulfil its functions.

1. Plane Sensor Thread  
   - This thread manages the IoT sensor data. Data is received, then posted via HTTPS to the database.

2. Heartbeat Manager Thread  
   - This thread is in charge of sending Command-in-Control a "heartbeat" which will allow the Command-in-Control to know if the gateway is turned on.  
   - This frequency is able to be changed by the Command-in-Control via MQTT.

3. CiC-Command Thread  
   - This thread manages all incoming order from Command-in-Control.  
   - This includes:
      1. Daily Diagnostics
      2. Custom Tests (Not fully implemented)
      3. Update Gateway Owner
      4. Update Gateway ID
      5. Update Gateway Location
      6. Update Heartbeat frequency
      
## Class Overviews

1. GWController.java  

- This is the only class with a main method, and is what needs to be run to turn on the Gateway Controller.
- Creates a GWController instance, that connects to the Gateway Diagnostic component.
- For this class to operate correctly, the Python program (*gateway_diagnostics.py*) that complements it must be  started first on the same local machine, to create a socket that   it can use to fulfill heartbeat requests and on demand test  requests.  
- All helper methods are used to order tests from the Gateway Diagnostics component.

2. GWControllerHTTPSHandler.java
    - This class is used to create an HTTPS connection when necessary to POST IoT sensor data, gateway heartbeats, or diagnostic test results. It is put into a separate class for clarity of code.
3. Drone.java
    - This class was designed by Oracle to facilitate MQTT protocols. It has been modified significantly to react operate on Mosquitto Test Broker, and to react specifically to the MQTT topics that were agreed upon for  communication between IoT Sensors and Command-in-Control.
    - This class allows the instantiation of 'Drone' objects that subscribe/publish to our preset MQTT Topics.

4. MessageActionListener.java
    - This class is short Oracle helper that facilitates the asynchronous tokens needed for MQTT communication.

5. IoT Sensors.java
    - This class implements Runnable in order to allow it to be used for multithreading. It creates the 3 MQTT drones 
    (*[Cabin Sensor]*, *[Engine Sensor]*, and *[External Sensor]*). This keeps the IoT sensor data and posting on a 
    thread by itself.
6. Heartbeat Manager.java
    - This class implements Runnable in order to allow it to be used for multithreading. It orders the gateway heartbeat from Gateway Diagnostics according to the GWController's heartbeat according to the *heartbeatFrequency* attribute. This keeps the heartbeat reporting separate from on-demand testing.

7. CiCReceiver.java
    - This class implements Runnable in order to allow it to be used for multithreading. It creates 1 MQTT drone, 
    *CiC-Command*. This keeps the Command-in-Control's on demand updates, requests & fulfillment in a separate thread.